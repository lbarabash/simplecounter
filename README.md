# Abstract

Simple counter that performs plus operations with integer numbers.

# Install

git clone git@bitbucket.org:lbarabash/simplecounter.git

cd simplecounter

npm install

npm start

Open http://localhost:3000/ in your browser.

# Details

Based on the https://github.com/davezuko/react-redux-starter-kit boilerplate repository.

Touched files in directories:

src/layouts/PageLayout

src/components

Everything else is not touched :)
